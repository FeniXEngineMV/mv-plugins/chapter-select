import { _Params } from './Core'

export default class extends Window_Selectable {
  constructor (rect) {
    /* #if _MV
    super(rect.x, rect.y, rect.width, rect.height)
    // #else */
    super(rect)
    // #endif
    this.useParamChapters = false
    this._page = 0
    this._data = []
    this._thumbnails = []
    this.contents.fontSize = _Params.chapterWindow.fontSize
    this.refresh()
  }

  update () {
    super.update()
  }

  refresh () {
    this.contents.clear()
    this.makeItemList()
    this.drawAllItems()
  }

  itemHeight (index) {
    return _Params.chapterWindow.itemHeight
  }

  item () {
    return this._data && this.index() >= 0 ? this._data[this.index()] : null
  }

  isCurrentItemEnabled () {
    return this.isEnabled(this.item())
  }

  isEnabled (item) {
    return item.lockState === true
  }

  makeItemList () {
    this._data = $gameSystem.chapters()
  }

  maxItems () {
    return _Params.chapterWindow.maxItems
  }

  drawTextRect (text, rect) {
    const y = rect.y + this.contents.fontSize + this.standardPadding()
    const width = this.textWidth(text) - 5
    this.contents.fillRect(rect.x + this.standardPadding(), y + 3, width + 25, 5, _Params.horizontalLineColor)
  }

  drawItem (index) {
    if (!this._data) return
    const chapter = this._data[index]

    if (chapter) {
      const rect = this.itemRect(index)
      this.changePaintOpacity(this.isEnabled(chapter))
      this.makeFontBigger()
      this.drawText(chapter.name, rect.x + this.standardPadding(), rect.y + this.standardPadding(), rect.width)
      this.drawTextRect(chapter.name, rect)
      this.makeFontSmaller()
      this.drawTextAutoWrap(chapter.description, rect.x + this.standardPadding(), rect.y + 50 + this.standardPadding(), rect.width - 8)
    }
  }
}
