import { autowrap } from '@fenixengine/tools'

Window_Base.prototype.standardFontFace = function () {
  if (!('isChinese' in $gameSystem)) {
    return 'GameFont'
  }
  if ($gameSystem.isChinese()) {
    return 'SimHei, Heiti TC, sans-serif'
  } else if ($gameSystem.isKorean()) {
    return 'Dotum, AppleGothic, sans-serif'
  } else {
    return 'GameFont'
  }
}

Window_Base.prototype.drawTextAutoWrap = function (text, x, y, width) {
  const charAmount = $gameSystem.isChinese() ? '  ' : ' '
  const charWidth = this.textWidth(charAmount)
  const maxCharactersPerLine = width / charWidth
  // we want to remove all line breaks from the text that were manually entered
  text = text.replace(/(\r\n|\n|\r)/gm, ' ')
  text = autowrap(text, maxCharactersPerLine, true).join('\n')
  Window_Base.prototype.drawTextEx.call(this, text, x, y)
}

Window_Base.prototype.standardPadding = function () {
  /* #if _MV
  return 12
  // #elif _MZ */
  return $gameSystem.windowPadding()
  // #endif
}
