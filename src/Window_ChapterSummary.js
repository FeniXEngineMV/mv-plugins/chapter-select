import { _Params } from './Core'

export default class extends Window_Base {
  constructor (rect) {
    /* #if _MV
    super(rect.x, rect.y, rect.width, rect.height)
    // #else */
    super(rect)
    // #endif
    this._chapter = null
  }

  setChapter (chapter) {
    this._chapter = chapter
    this.refresh()
  }

  update () {
    super.update()
  }

  refresh () {
    this.contents.clear()
    this.drawTitle()
    if (this._chapter) {
      this.drawChapterSummary()
    }
  }

  makeFontSmaller () {
    if (this.contents.fontSize >= 24) {
      this.contents.fontSize -= 8
    }
  }

  drawTitle () {
    this.drawText(_Params.summaryTitle, 0, 0, this.width - this.standardPadding(), 'center')
    this.contents.fillRect(0, 35, this.width, 5, _Params.horizontalLineColor)
  }

  drawChapterSummary () {
    const chapter = this._chapter
    this.drawTextAutoWrap(chapter.summary, 0, 45, this.contents.width)
    this.resetFontSettings()
  }
}
