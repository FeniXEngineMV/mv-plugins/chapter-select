import { _Params } from './Core'
import Scene_ChapterSelect from './Scene_ChapterSelect'

const createCommandWindow = Scene_Title.prototype.createCommandWindow
Scene_Title.prototype.createCommandWindow = function () {
  createCommandWindow.call(this)
  if (_Params.isChapterCommandEnabled) {
    this._commandWindow.setHandler('chapterSelect', this.commandChapterSelect.bind(this))
  }
}

Scene_Title.prototype.commandChapterSelect = function () {
  this._commandWindow.close()
  SceneManager.push(Scene_ChapterSelect)
}
