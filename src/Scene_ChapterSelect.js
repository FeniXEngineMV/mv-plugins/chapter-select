import { _Params } from './Core'
import Window_ChapterSelect from './Window_ChapterSelect'
import Window_ChapterThumbnail from './Window_ChapterThumbnail'
import Window_ChapterSummary from './Window_ChapterSummary'

export default class extends Scene_MenuBase {
  constructor () {
    super()
    super.initialize()
    this._selectedChapter = 0
    this._loadSuccess = false
  }

  // #if _MZ
  isBottomHelpMode () {
    return false
  }
  // #endif

  // #if _MV
  helpWindowRect () {
    const wx = 0
    const wy = 0
    const ww = Graphics.boxWidth
    const wh = Window_Base.prototype.fittingHeight(2)
    return new Rectangle(wx, wy, ww, wh)
  }

  buttonAreaHeight () {
    return 0
  }
  // #endif

  create () {
    super.create()
    DataManager.createGameObjects()
    DataManager.loadAllSavefileImages()
    this.createChapterBackground()
    this.createHelpWindow()
    this.createSaveFileList()
    this.createChapterSelect()
    this.createChapterThumbnail()
    this.createChapterSummary()
  }

  start () {
    super.start()
    if (this._saveFileList) {
      this._saveFileList.refresh()
    }
    if (_Params.loadLatestSave && !_Params.chapterWithoutSave) {
      // #if _MV
      if (this.loadSaveMv(DataManager.latestSavefileId())) {
        this._loadSuccess = true
        this._helpWindow.setText(_Params.helpWindowTerm)
        this._chapterSelectWindow.refresh()
      }
      // #endif
      // #if _MZ
      DataManager.loadGame(DataManager.latestSavefileId())
        .then(() => {
          this._loadSuccess = true
          this._helpWindow.setText(_Params.helpWindowTerm)
          this._chapterSelectWindow.refresh()
        })
      // #endif
    }
  }

  update () {
    super.update()
    if ((_Params.loadLatestSave || _Params.chapterWithoutSave) && !this._chapterSelectWindow.open()) {
      this._thumbnailWindow.open()
      this._summaryWindow.open()
      this._chapterSelectWindow.open()
      this._chapterSelectWindow.activate()
      this._helpWindow.setText(_Params.helpWindowTerm)
      this._chapterSelectWindow.refresh()
    }
    if ((!_Params.loadLatestSave && !_Params.chapterWithoutSave) && !this._saveFileList.isClosing() && this._saveFileList.isClosed() && !this._chapterSelectWindow.isOpen()) {
      this._thumbnailWindow.open()
      this._summaryWindow.open()
      this._chapterSelectWindow.open()
      this._chapterSelectWindow.activate()
    }
    if (this._selectedChapter !== this._chapterSelectWindow.index()) {
      this._selectedChapter = this._chapterSelectWindow.index()
      this._thumbnailWindow.setChapter(this.selectedChapter())
      this._summaryWindow.setChapter(this.selectedChapter())
    }
  }

  createChapterBackground () {
    this._chapterBackground = new Sprite()
    this.addChild(this._chapterBackground)
  }

  chapterSelectRect () {
    return new Rectangle(
      0,
      this._tryEval(_Params.chapterWindow.y),
      this._tryEval(_Params.chapterWindow.width),
      this._tryEval(_Params.chapterWindow.height)
    )
  }

  createChapterSelect () {
    const rect = this.chapterSelectRect()

    this._chapterSelectWindow = new Window_ChapterSelect(rect)
    this._chapterSelectWindow.setHandler('ok', this.onChapterSelect.bind(this))
    this._chapterSelectWindow.setHandler('cancel', this.popScene.bind(this))
    this._chapterSelectWindow.close()
    if (_Params.chapterWithoutSave) {
      this._chapterSelectWindow.useParamChapters = true
    }
    this.addWindow(this._chapterSelectWindow)
  }

  chapterThumbnailRect () {
    const options = _Params.thumbnailWindow
    return new Rectangle(
      this._tryEval(options.x),
      this._tryEval(options.y),
      this._tryEval(options.width),
      this._tryEval(options.height)
    )
  }

  createChapterThumbnail () {
    const rect = this.chapterThumbnailRect()

    this._thumbnailWindow = new Window_ChapterThumbnail(rect)
    this._thumbnailWindow.close()
    this.addWindow(this._thumbnailWindow)
  }

  chapterSummaryRect () {
    const options = _Params.summaryWindow
    return new Rectangle(
      this._tryEval(options.x),
      this._tryEval(options.y),
      this._tryEval(options.width),
      this._tryEval(options.height)
    )
  }

  createChapterSummary () {
    const rect = this.chapterSummaryRect()
    this._summaryWindow = new Window_ChapterSummary(rect)
    this._summaryWindow.close()
    this.addWindow(this._summaryWindow)
  }

  saveFilelistRect () {
    const helpRect = this.helpWindowRect()
    return new Rectangle(
      0,
      helpRect.bottom,
      Graphics.boxWidth,
      Graphics.boxHeight - (helpRect.height + this.buttonAreaHeight())
    )
  }

  createSaveFileList () {
    if (_Params.loadLatestSave || _Params.chapterWithoutSave) {
      return
    }
    const rect = this.saveFilelistRect()

    // #if _MV
    this._saveFileList = new Window_SavefileList(0, rect.y, rect.width, rect.height)
    // #else
    this._saveFileList = new Window_SavefileList(rect)
    // #endif
    this._saveFileList.setHandler('ok', this.onSaveListOk.bind(this))
    this._saveFileList.setHandler('cancel', this.popScene.bind(this))
    this._saveFileList.select(0)
    this._saveFileList.setTopRow(0 - 2)
    this._helpWindow.setText('Load a save file')
    this.addWindow(this._saveFileList)
  }

  selectedChapter () {
    return this._chapterSelectWindow.item()
  }

  onSaveFileLoaded () {
    this._loadSuccess = true
    this._saveFileList.deactivate()
    this._saveFileList.close()
    this._helpWindow.setText(_Params.helpWindowTerm)
    this._chapterSelectWindow.refresh()
  }

  onSaveListOk () {
    const savefileId = this._saveFileList.index() + 1
    // #if _MV
    if (this.loadSaveMv(savefileId)) {
      this.onSaveFileLoaded()
    } else {
      this._saveFileList.activate()
    }
    // #endif
    // #if _MZ
    DataManager.loadGame(savefileId)
      .then(() => {
        this.onSaveFileLoaded()
      })
      .catch(() => {
        this._saveFileList.activate()
      })
    // #endif
  }

  loadSaveMv (savefileId) {
    if (DataManager.loadGame(savefileId)) {
      SoundManager.playLoad()
      return true
    } else {
      SoundManager.playBuzzer()
      return false
    }
  }

  onChapterSelect () {
    const chapter = this.selectedChapter()
    this._chapterSelectWindow.close()
    this.fadeOutAll()
    if (chapter.requiredVariables || chapter.requiredSwitches) {
      $gameTemp.needsChapterSetup = true
      $gameTemp.chapterId = chapter.id
    }
    if (_Params.chapterWithoutSave) {
      DataManager.setupNewGame()
    }
    $gamePlayer.reserveTransfer(chapter.startMapId, chapter.playerX, chapter.playerY)
    $gamePlayer.requestMapReload()
    SceneManager.goto(Scene_Map)
  }

  _tryEval (expression) {
    try {
      // eslint-disable-next-line no-eval
      return eval(expression)
    } catch (error) {
      console.error(`Unable to evaluate the following expression ${expression}`)
    }
  }
}
