// #if _PRO
// #endif

import { convertParameters } from 'fenix-tools'

export const pluginName = document.currentScript.src.match(/.+\/(.+).js/)[1]

export const rawParameters = PluginManager.parameters(pluginName)

export const _Params = convertParameters(rawParameters)
