import { _Params } from './Core'

const aliasMakeCommandList = Window_TitleCommand.prototype.makeCommandList
Window_TitleCommand.prototype.makeCommandList = function () {
  aliasMakeCommandList.call(this)
  this.addCommand(_Params.chapterCommandText, 'chapterSelect', this.isContinueEnabled())
}
