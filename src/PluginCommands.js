import Scene_ChapterSelect from './Scene_ChapterSelect'
import { pluginName } from './Core'

// #if _MZ
PluginManager.registerCommand(pluginName, 'open', args => {
  SceneManager.push(Scene_ChapterSelect)
})

PluginManager.registerCommand(pluginName, 'lock', args => {
  $gameSystem.lockChapter(args.chapterId)
})

PluginManager.registerCommand(pluginName, 'unlock', args => {
  $gameSystem.unlockChapter(args.chapterId)
})

PluginManager.registerCommand(pluginName, 'updateChapters', args => {
  const keepLockState = args.keepLockState
  $gameSystem.updateChapters(keepLockState)
})
// #endif

// #if _MV
const pluginCommand = Game_Interpreter.prototype.pluginCommand
Game_Interpreter.prototype.pluginCommand = function (command, args) {
  if (command.toLowerCase() === 'chapter') {
    const subCommand = args[0].toLowerCase()
    switch (subCommand) {
      case 'lock':
        $gameSystem.lockChapter(args[1])
        break
      case 'unlock':
        $gameSystem.unlockChapter(args[1])
        break
      case 'open':
        SceneManager.push(Scene_ChapterSelect)
        break
      case 'update':
        $gameSystem.updateChapters(args[1] === 'true')
        break
      default:
        console.error(`There was a problem with plugin command ${command} ${subCommand}`)
        break
    }
  } else {
    pluginCommand.call(this, command, args)
  }
}
// #endif
