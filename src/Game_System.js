import { _Params } from './Core'
const initialize = Game_System.prototype.initialize
Game_System.prototype.initialize = function () {
  initialize.call(this)
  this._chapters = []
  this.setupChapters()
}

Game_System.prototype.setupChapters = function () {
  _Params.chapters.forEach((chapter, index) => {
    this._chapters.push({
      id: index + 1,
      ...chapter
    })
  })
}

Game_System.prototype.updateChapters = function (keepLockState) {
  const oldChapters = this._chapters.clone()
  this._chapters = []
  _Params.chapters.forEach((chapter, index) => {
    const oldChapter = oldChapters[index]
    this._chapters.push({
      id: index + 1,
      ...chapter,
      lockState: keepLockState && oldChapter ? oldChapter.lockState : chapter.lockState
    })
  })
}

Game_System.prototype.chapters = function () {
  return this._chapters
}

Game_System.prototype.getChapterById = function (chapterId) {
  return this._chapters[chapterId - 1]
}

Game_System.prototype.getChapterDescription = function (chapterId) {
  return this._chapters[chapterId - 1].description
}

Game_System.prototype.isChapterLocked = function (chapterId) {
  return this._chapters[chapterId - 1].lockState === false
}

// A locked chapter's lockState will be set to false
Game_System.prototype.lockChapter = function (chapterId) {
  this._chapters[chapterId - 1].lockState = false
}

// An unlocked chapter's lockState will be set to true
Game_System.prototype.unlockChapter = function (chapterId) {
  this._chapters[chapterId - 1].lockState = true
}
