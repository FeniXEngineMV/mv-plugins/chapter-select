const aliasSetup = Game_Map.prototype.setup
Game_Map.prototype.setup = function (mapId) {
  aliasSetup.call(this, mapId)

  if ($gameTemp.needsChapterSetup) {
    const chapter = $gameSystem.getChapterById($gameTemp.chapterId)
    const switchData = chapter.requiredSwitches
    const variableData = chapter.requiredVariables

    $gameTemp.needsChapterSetup = false
    $gameTemp.chapterId = 0

    if (switchData && switchData.length > 0) {
      switchData.forEach(data => {
        $gameSwitches.setValue(data.id, data.value)
      })
    }

    if (variableData && variableData.length > 0) {
      variableData.forEach(data => {
        $gameVariables.setValue(data.id, data.value)
      })
    }
  }
}
