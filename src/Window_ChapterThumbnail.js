export default class extends Window_Base {
  constructor (rect) {
    /* #if _MV
    super(rect.x, rect.y, rect.width, rect.height)
    // #else */
    super(rect)
    // #endif
    this._chapter = null
    this.createThumbnail()
  }

  createThumbnail () {
    this._thumbnail = new Sprite()
    this._thumbnail.x = 0 + this.standardPadding()
    this._thumbnail.y = 0 + this.standardPadding()
    this._thumbnail.width = this.width / 2
    this._thumbnail.height = this.height / 2
    this.addChild(this._thumbnail)
  }

  setChapter (chapter) {
    this._chapter = chapter
    this.refresh()
  }

  update () {
    super.update()
  }

  drawTextRect (text, rect) {
    const y = rect.y + this.contents.fontSize
    const width = this.textWidth(text)
    this.contents.fillRect(rect.x, y, width + 25, 5, '#fcefb3')
  }

  refresh () {
    const chapter = this._chapter
    if (chapter && chapter.thumbnail) {
      this._thumbnail.bitmap = ImageManager.loadChapterThumbnail(chapter.thumbnail)
      this._thumbnail.bitmap.addLoadListener((bitmap) => {
        this._thumbnail.width = this.contents.width
        this._thumbnail.height = this.contents.height
      })
    }
  }
}
