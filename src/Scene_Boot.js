import { _Params } from './Core'

const loadSystemImages = Scene_Boot.prototype.loadSystemImages
Scene_Boot.prototype.loadSystemImages = function () {
  loadSystemImages.call(this)
  const thumbnailPaths = _Params.chapters.map(c => c.thumbnail)
  thumbnailPaths.forEach(path => {
    ImageManager.reserveChapterThumbnail(path)
  })
}
