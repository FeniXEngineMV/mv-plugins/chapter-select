function parsePath (path) {
  let folder = ''
  let filename = ''
  const paths = path.split('/')

  if (paths.length > 2) {
    paths.forEach((p, index) => {
      if (index === paths.length - 1) {
        filename = p
        return
      }
      folder += `${p}/`
    })
  } else {
    folder = `img/${paths[0]}/`
    filename = paths[1]
  }

  return { folder, filename }
}

ImageManager.loadChapterThumbnail = function (path) {
  const { filename, folder } = parsePath(path)

  return this.loadBitmap(folder, filename, null, true)
}

ImageManager.reserveChapterThumbnail = function (path, hue, reservationId) {
  const { filename, folder } = parsePath(path)
  // #if _MV
  this.reserveBitmap(folder, filename, hue, false, reservationId)
  // #else
  this.loadChapterThumbnail(`${folder}${filename}`)
  // #endif
}
